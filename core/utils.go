package core

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"github.com/hashicorp/logutils"
	"github.com/spf13/viper"

	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	jaegercfg "github.com/uber/jaeger-client-go/config"
)

// config hashilogger from debug flag writing to stderr
func SetupLogger() {
	var loglevel string
	if viper.GetBool("debug") {
		loglevel = "DEBUG"
	} else {
		loglevel = "INFO"
	}
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "ERROR"},
		MinLevel: logutils.LogLevel(loglevel),
		Writer:   os.Stderr,
	}
	log.SetOutput(filter)
}

func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func GetRemoteUser(r *http.Request) (string, bool) {

	user := r.Header.Get("REMOTE_USER")

	authorised_users := viper.GetStringSlice(SERVE_ACL_KEY)
	if user == "" { // not set
		return user, false
	}
	for _, u := range authorised_users {
		if strings.TrimSpace(u) == user {
			return user, true
		}
	}
	return user, false
}

func RequireUserIsAdmin(w http.ResponseWriter, r *http.Request, methodName string) (requestedUser string, authenticatedUser string, isAdmin bool, ok bool) {

	vars := mux.Vars(r)
	requestedUser = vars["user"] // from URL
	authenticatedUser, isAdmin = GetRemoteUser(r)

	if !isAdmin {
		ok = false
		LogAndSetHttpStatus(w, r, http.StatusUnauthorized, methodName, "")
		return
	} else {
		ok = true
		return
	}
}

func RequireUserIsAdminOrDealingWithSelf(w http.ResponseWriter, r *http.Request, methodName string) (requestedUser string, authenticatedUser string, isAdmin bool, ok bool) {

	vars := mux.Vars(r)
	requestedUser = vars["user"] // from URL
	authenticatedUser, isAdmin = GetRemoteUser(r)

	if requestedUser != authenticatedUser && !isAdmin {
		ok = false
		LogAndSetHttpStatus(w, r, http.StatusUnauthorized, methodName, "")
		return
	} else {
		ok = true
		return
	}
}

func LogAndSetHttpStatus(w http.ResponseWriter, r *http.Request, httpstatus int, methodName string, optionalMessage string) {
	bracketedExplainer := ""
	if optionalMessage == "" {
		optionalMessage = http.StatusText(httpstatus)
	} else {
		bracketedExplainer = fmt.Sprintf(" [%s]", optionalMessage)
	}

	authUser, _ := GetRemoteUser(r)

	// log it
	log.Printf("[INFO] %s request %s %s %s (%s) %d %s%s", methodName, r.Method, r.URL.Path, r.RemoteAddr,
		authUser, httpstatus, http.StatusText(httpstatus), bracketedExplainer)

	// set http error on writer
	if httpstatus != http.StatusOK {
		http.Error(w, optionalMessage, httpstatus)
	}
}

// initialising Jaeger client
func InitTracing() (error, opentracing.Tracer, io.Closer) {

	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		// parsing errors might happen here, such as when we get a string where we expect a number
		log.Printf("Could not parse Jaeger env vars: %s", err.Error())
		return err, nil, nil
	}

	tracer, closer, err := cfg.NewTracer()
	if err != nil {
		log.Printf("Could not initialize jaeger tracer: %s", err.Error())
		return err, nil, nil
	}
	log.Printf("Initialised Jaeger for service : %q", cfg.ServiceName)
	return nil, tracer, closer
}

// Extracts Span from headers or returns a root one
func ExtractSpanFromHeaders(operationName string, r *http.Request) opentracing.Span {
	carrier := opentracing.HTTPHeadersCarrier(r.Header)
	tracer := opentracing.GlobalTracer()
	clientContext, _ := tracer.Extract(opentracing.HTTPHeaders, carrier)
	span := tracer.StartSpan(operationName, ext.RPCServerOption(clientContext))
	ext.HTTPMethod.Set(span, r.Method)
	ext.HTTPUrl.Set(span, r.RequestURI)
	return span
}
