package core

import "github.com/spf13/viper"

func IsPoolOK(poolName string) bool {
	return StringInSlice(poolName, viper.GetStringSlice(INTERNAL_POOLS))
}

func IsOnStandardList(scheddName string, poolName string) bool {
	pools := viper.Get(INTERNAL_POOLMAP)
	pool, ok := pools.(*PoolMap).Pools[poolName]
	if ! ok {
		return false // bad pool
	}
	if StringInSlice(scheddName, pool.Standard) {
		return true
	} else {
		return false
	}
}

func IsOnRestrictedList(scheddName string, poolName string) bool {
	pools := viper.Get(INTERNAL_POOLMAP)
	pool, ok := pools.(*PoolMap).Pools[poolName]
	if ! ok {
		return false // bad pool
	}
	if StringInSlice(scheddName, pool.Restricted) {
		return true
	} else {
		return false
	}
}