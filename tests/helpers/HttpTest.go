//+build !test

package helpers

import (
	"bytes"
	"context"
	"crypto/tls"
	"fmt"
	"github.com/stretchr/testify/assert"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

type WTFISResponse struct {
	JsonBody 	PartResponse
	StatusCode 	PartResponse
}

type PartResponse struct {
	Field interface{}
	Enabled bool
}

type HttpTest struct {
	Name                   string
	SetUp, TearDown        func(t *testing.T)
	Method, Endpoint, Data string
	ExpectedResponse       WTFISResponse
	Context                context.Context
	RemoteUserHeader       string
	IgnoreCertificate	   bool
}

func (tC HttpTest) Run(t *testing.T) {
	assert.NotEmptyf(t, tC.Name, "The [HttpTest] requires a non-empty [Name] field to be given")

	if tC.SetUp != nil 		{ tC.SetUp(t) }
	if tC.TearDown != nil 	{ defer tC.TearDown(t) }

	reqBody := bytes.NewBuffer([]byte(tC.Data))
	req, err := http.NewRequest(tC.Method, tC.Endpoint, reqBody)
	assert.NoErrorf(t, err, "unable to create the HTTP request. Error [%v]", err)
	if tC.Context != nil {
		req.WithContext(tC.Context)
	}

	if len(tC.RemoteUserHeader) != 0 {
		t.Logf("Setting the [REMOTE_USER] HTTP request header to [%s]...", tC.RemoteUserHeader)
		req.Header.Set("REMOTE_USER", tC.RemoteUserHeader)
	}
	client := http.DefaultClient
	if tC.IgnoreCertificate {
		t.Log("Ignoring the x509 certificate...")
		tr := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}
		client = &http.Client{Transport:tr}
	}

	//err = negotiate.Negotiate(req)
	//assert.NoErrorf(t, err, "unable to negotiate the current kerberos token. Error [%v]", err)

	res, err := client.Do(req)
	if err != nil {	t.Fatal(err); t.FailNow() }

	assert.NoErrorf(t, err, "an error was detected when executing the HTTP request [%+v]", req)
	assert.NotNil(t, res)

	if tC.ExpectedResponse.StatusCode.Enabled {
		assert.Equalf(t, tC.ExpectedResponse.StatusCode.Field, res.StatusCode,
			"expected status code [%d] but got [%+v] instead", tC.ExpectedResponse.StatusCode.Field, res)
	}

	if tC.ExpectedResponse.JsonBody.Enabled {
		body, err := readResponseAsString(res)
		assert.NoError(t, err)
		assert.NotNil(t, body)
		assert.Equalf(t, strings.ToLower(tC.ExpectedResponse.JsonBody.Field.(string)), strings.ToLower(body),
			"expected JSON body [%s] but got [%+v] instead", tC.ExpectedResponse.JsonBody.Field, res)
	}
}

// ReadResponseBody : Helper function that encapsulates the logic of reading the [http.Response.Body] as a string
func readResponseAsString(response *http.Response) (string, error) {
	if response == nil {
		return "", fmt.Errorf("the given response instance pointer cannot be null")
	}
	bBytes, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", err
	}
	return string(bBytes), nil
}