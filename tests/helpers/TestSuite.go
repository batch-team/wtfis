//+build !test

package helpers

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func Run(t *testing.T, test HttpTest) {
	assert.NotNil(t, test)
	t.Run(test.Name, test.Run)
}

func RunMultiple(t *testing.T, tests ...HttpTest) {
	assert.NotNil(t, tests)
	assert.NotEmpty(t, tests)

	for _, test := range tests {
		Run(t, test)
	}
}
