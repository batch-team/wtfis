package core

type GetUserMessage struct {
	User          string     `json:"user"`
	Pool          string     `json:"pool"`
	CurrentSchedd string     `json:"currentschedd"`
	OldSchedds    []string   `json:"oldschedds"`
}

type ScheddUsers struct {
	Pool          string     `json:"pool"`
	Schedd        string     `json:"schedd"`
    Users         []string   `json:"users"`
}

type PutUserMessage struct {
	User          string     `json:"user"`
	Pool          string     `json:"pool"`
	CurrentSchedd string     `json:"currentschedd"`
}

type ActiveUserMessage struct {
	User          string     `json:"user"`
	Pool          string     `json:"pool"`
	ActiveSchedds []string   `json:"activeschedds"`
}
