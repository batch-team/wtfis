package serve

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	// "github.com/go-zookeeper/zk"
	"github.com/gorilla/mux"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

func handlePutUser(w http.ResponseWriter, r *http.Request) {
	span := core.ExtractSpanFromHeaders("PutUser", r)
	defer span.Finish()

	requestedUser, authUser, admin, passedok := core.RequireUserIsAdminOrDealingWithSelf(w, r, "PutUser")
	if !passedok {
		return
	}
	requestedPool := mux.Vars(r)["pool"]

	span.SetTag("user", requestedUser)
	span.SetTag("pool", requestedPool)

	// decode message and 500 if it's bad
	var putMessage core.PutUserMessage
	if r.Body == nil { //todo: actually wrong
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "PutUser", "Missing message body")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&putMessage)
	if err != nil {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "PutUser", "Invalid JSON body")
		span.SetTag("error", true)
		return
	}

	// check all the required fields are set and 500 if they are not
	if putMessage.CurrentSchedd == "" || putMessage.User == "" || putMessage.Pool == "" {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "PutUser", "Not all required fields passed in JSON")
		return
	}

	// check the user in body matches the user requested and 500 if not
	if putMessage.User != requestedUser {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "PutUser", "User passed in JSON does not match user in PUT request URL.")
		return
	}

	// check the pool in body matches the pool requested and 500 if not
	if putMessage.Pool != requestedPool {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "PutUser", "Pool passed in JSON does not match pool in PUT request URL.")
		return
	}

	// validate the pool requested is in the list
	if !core.IsPoolOK(requestedPool) {
		out := fmt.Sprintf("Requested pool (%s) is not known", requestedPool)
		core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "PutUser", out)
		return
	}

	// validate the new schedd is on da list
	foundinstandardlist := core.IsOnStandardList(putMessage.CurrentSchedd, requestedPool)
	foundinrestrictedlist := core.IsOnRestrictedList(putMessage.CurrentSchedd, requestedPool)

	if admin {
		// admin users schedds must be on at least one of the lists
		if (!foundinrestrictedlist) && (!foundinstandardlist) {
			out := fmt.Sprintf("Requested schedd (%s) is not known", putMessage.CurrentSchedd)
			core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "PutUser", out)
			return
		}
	} else {
		// normal users can update a standard schedd but not a restricted one
		if foundinrestrictedlist {
			out := fmt.Sprintf("Authenticated user (%s) is not authorized to update a user with schedd (%s)", authUser, putMessage.CurrentSchedd)
			core.LogAndSetHttpStatus(w, r, http.StatusUnauthorized, "PutUser", out)
			return
		}
		// normal users can only update a standard schedd
		if !foundinstandardlist {
			out := fmt.Sprintf("Requested schedd (%s) is not known", putMessage.CurrentSchedd)
			core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "PutUser", out)
			return
		}
	}

	// relevant znodes
	baseZnode := fmt.Sprintf("%s/%s/users/%s", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)
	currentZnode := fmt.Sprintf("%s/%s/users/%s/current", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)
	previousZnode := fmt.Sprintf("%s/%s/users/%s/previous", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)

	// get old value, will be zero value on non-existing
	oldCurrent, _, _ := getZNode(currentZnode, span)
	oldPrevious, _, _ := getZNode(previousZnode, span)

	// oldPrevious is whitespace separated list of hostnames
	oldPreviousArray := strings.Fields(string(oldPrevious))

	// set new current to requested value
	newCurrent := putMessage.CurrentSchedd

	// create new previous array to the Set(old_Previous + oldCurrent - newCurrent)
	pset := make(map[string]int)
	for _, p := range oldPreviousArray {
		pset[p] = 0
	}
	pset[string(oldCurrent)] = 0
	delete(pset, newCurrent)

	newPreviousArray := make([]string, 0)
	for p := range pset {
		newPreviousArray = append(newPreviousArray, p)
	}

	// ..back to space separated string
	newPrevious := strings.Join(newPreviousArray, " ")

	// blat them into place with multi
	baseOp := createOrSetOp(baseZnode, "")
	currentOp := createOrSetOp(currentZnode, newCurrent)
	previousOp := createOrSetOp(previousZnode, newPrevious)

	mr, err := zoo.Multi(baseOp, currentOp, previousOp)
	if err != nil {
		core.LogAndSetHttpStatus(w, r, http.StatusInternalServerError, "PutUser", "")
		log.Printf("[ERROR] Error writing ZK nodes: %v", err)
		for i, sadop := range mr {
			log.Printf("[ERROR]   (%d): %v", i, sadop.Error)
		}
		return
	}

	core.LogAndSetHttpStatus(w, r, http.StatusOK, "PutUser", "")
	ext.HTTPStatusCode.Set(span, http.StatusOK)
	log.Printf("[DEBUG]  PutUser updated (%s) to (%s)", currentZnode, newCurrent)
	log.Printf("[DEBUG]  PutUser updated (%s) to (%s)", previousZnode, newPrevious)

}
