package core

const (
	SERVER_VERSION = "0.2"
)

type wtfisctx string

var WG_CTX_KEY wtfisctx

const (
	SERVE_PORT_KEY = "serve.port"
	SERVE_ZK_CONN  = "serve.zkconn"
	SERVE_ACL_KEY  = "serve.acl"
	SERVE_ROOTPATH = "serve.rootpath"

	AUTHZ_GETMODE = "authz.getmode"

	INTERNAL_POOLS   = "xipools"
	INTERNAL_POOLMAP = "xipoolmap"
)
