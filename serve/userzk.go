package serve

import (
	"github.com/go-zookeeper/zk"
)

func createOrSetOp(zNode string, value string) interface{} {

	mappingACL := zk.WorldACL(zk.PermAll)
	exists, stat, _ := zoo.Exists(zNode)

	var op interface{}
	if exists {
		op = &zk.SetDataRequest{Path: zNode, Data: []byte(value), Version: stat.Version}
	} else {
		op = &zk.CreateRequest{Path: zNode, Data: []byte(value), Acl: mappingACL, Flags: 0}
	}

	return op
}
