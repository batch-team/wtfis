package core

import (
	"fmt"
	"log"
	"time"

	"github.com/go-zookeeper/zk"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type NullLogger struct{}

func (n NullLogger) Printf(s string, i ...interface{}) { return }

func ConnectZK() (*zk.Conn, error) {
	zkConns := viper.GetStringSlice(SERVE_ZK_CONN)

	// todo: infinite retry on no connection is problematic, and very noisy in the DEBUG logs
	client, events, err := zk.Connect(zkConns, time.Second, zk.WithLogger(NullLogger{}))
	if err != nil {
		return nil, errors.Wrap(err, "could not connect with [zookeeper]")
	}

	// List to ZK events
	go listenToZkEvents(events)

	// Create all the required paths
	err = createBasePaths(client)
	if err != nil {
		return nil, err
	}

	return client, nil
}

func listenToZkEvents(events <-chan zk.Event) {
	for event := range events {
		log.Printf("[DEBUG] Received ZK event [%+v]", event)
	}
}

func createBasePaths(client *zk.Conn) error {

	// Create the main base path
	err := createZkPath(client, viper.GetString(SERVE_ROOTPATH), zk.WorldACL(zk.PermAll))
	if err != nil {
		return err
	}

	// Creat the pool specific paths
	for _, pool := range viper.GetStringSlice(INTERNAL_POOLS) {
		// Create the users path
		usersPath := fmt.Sprintf("%s/%s", viper.GetString(SERVE_ROOTPATH), pool)
		err = createZkPath(client, usersPath, zk.WorldACL(zk.PermAll))
		if err != nil {
			return err
		}
		usersPath = fmt.Sprintf("%s/%s/users", viper.GetString(SERVE_ROOTPATH), pool)
		err = createZkPath(client, usersPath, zk.WorldACL(zk.PermAll))
		if err != nil {
			return err
		}
	}
	return nil
}

func createZkPath(client *zk.Conn, path string, acls []zk.ACL) error {
	log.Printf("[INFO] Checking if the path [%s] has already been created...", path)
	_, _, err := client.Get(path)
	if err != nil {
		log.Printf("[INFO] The path [%s] was not found. Creating...", path)
		_, err := client.Create(path, []byte{}, 0, acls)
		if err != nil {
			return errors.Wrapf(err, "could not create a connection to the path [%s]", path)
		}
	} else {
		log.Printf("[DEBUG] The path [%s] found in the current cluster.", path)
	}
	return nil
}
