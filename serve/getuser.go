package serve

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/go-zookeeper/zk"
	"github.com/gorilla/mux"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

func getZNode(znode string, span opentracing.Span) ([]byte, *zk.Stat, error) {
	span = opentracing.GlobalTracer().StartSpan(
		"get-znode",
		opentracing.ChildOf(span.Context()),
	)
	defer span.Finish()

	span.SetTag("znode", znode)
	return zoo.Get(znode)
}

func handleGetUser(w http.ResponseWriter, r *http.Request) {
	span := core.ExtractSpanFromHeaders("GetUser", r)
	defer span.Finish()

	// choose correct authz mode
	authzmode := viper.GetString(core.AUTHZ_GETMODE)
	var requestedUser string
	var passedok bool

	if authzmode == "all" { // any user can query anybody
		requestedUser = mux.Vars(r)["user"]
	} else { // only admins can query others
		requestedUser, _, _, passedok = core.RequireUserIsAdminOrDealingWithSelf(w, r, "GetUser")
		if !passedok {
			return
		}
	}

	requestedPool := mux.Vars(r)["pool"]

	span.SetTag("user", requestedUser)
	span.SetTag("pool", requestedPool)

	baseZnode := fmt.Sprintf("%s/%s/users/%s", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)
	userZnode := fmt.Sprintf("%s/current", baseZnode)
	previousZnode := fmt.Sprintf("%s/previous", baseZnode)

	// validate the pool requested is in the list
	if !core.IsPoolOK(requestedPool) {
		out := fmt.Sprintf("Requested pool (%s) is not known", requestedPool)
		core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "GetUser", out)
		return
	}

	// get current znode
	contents, _, err := getZNode(userZnode, span)
	if err != nil {
		if err == zk.ErrNoNode {
			out := fmt.Sprintf("Requested user (%s) is not known in pool %s", requestedUser, requestedPool)
			core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "GetUser", out)
			return
		} else {
			errMsg := fmt.Sprintf("%v", err)
			core.LogAndSetHttpStatus(w, r, http.StatusInternalServerError, "GetUser", errMsg)
			return
		}
	}

	currentSchedd := string(contents)

	// get previous znode
	previous, _, _ := getZNode(previousZnode, span)
	previousArray := strings.Fields(string(previous))

	// construct and send back json
	gm := core.GetUserMessage{User: requestedUser, Pool: requestedPool, CurrentSchedd: currentSchedd, OldSchedds: previousArray}

	j := json.NewEncoder(w)
	j.Encode(gm)

	core.LogAndSetHttpStatus(w, r, http.StatusOK, "GetUser", "")
	ext.HTTPStatusCode.Set(span, http.StatusOK)
}
