package serve

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/go-zookeeper/zk"
	"github.com/gorilla/mux"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

func getZChildren(znode string, span opentracing.Span) ([]string, *zk.Stat, error) {
	span = opentracing.GlobalTracer().StartSpan(
		"children-znode",
		opentracing.ChildOf(span.Context()),
	)
	defer span.Finish()

	span.SetTag("znode", znode)
	return zoo.Children(znode)
}

func handleGetSchedd(w http.ResponseWriter, r *http.Request) {
	span := core.ExtractSpanFromHeaders("GetSchedd", r)
	defer span.Finish()

	_, _, _, passedok := core.RequireUserIsAdmin(w, r, "GetSchedd")
	if !passedok {
		return
	}
	requestedPool := mux.Vars(r)["pool"]
	requestedSchedd := mux.Vars(r)["schedd"]

	span.SetTag("user", requestedSchedd)
	span.SetTag("pool", requestedPool)

	// full scan
	uarray := make([]string, 0)
	baseZnode := fmt.Sprintf("%s/%s/users", viper.GetString(core.SERVE_ROOTPATH), requestedPool)
	userlist, _, err := getZChildren(baseZnode, span)
	if err != nil {
		core.LogAndSetHttpStatus(w, r, http.StatusInternalServerError, "GetSchedd", "")
		log.Printf("[ERROR]  %v", err)
		span.SetTag("error", true)
		return
	} else {
		log.Printf("[DEBUG] GetSchedd iterating over %d users in pool %s", len(userlist), requestedPool)
	}
	for _, username := range userlist {
		upath := fmt.Sprintf("%s/%s/current", baseZnode, username)
		data, _, err := getZNode(upath, span)
		if err != nil {
			log.Printf("[DEBUG] GetSchedd: Unexpected read fail for path: %s", upath)
			continue
		}
		if string(data) == requestedSchedd {
			uarray = append(uarray, string(username))
		}
	}

	// construct and send back json
	su := core.ScheddUsers{Schedd: requestedSchedd, Pool: requestedPool, Users: uarray}

	j := json.NewEncoder(w)
	j.Encode(su)

	core.LogAndSetHttpStatus(w, r, http.StatusOK, "GetSchedd", "")
	ext.HTTPStatusCode.Set(span, http.StatusOK)
}
