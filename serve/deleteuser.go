package serve

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-zookeeper/zk"
	"github.com/gorilla/mux"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

func handleDeleteUser(w http.ResponseWriter, r *http.Request) {
	span := core.ExtractSpanFromHeaders("DeleteUser", r)
	defer span.Finish()

	requestedUser, _, _, passedok := core.RequireUserIsAdmin(w, r, "DeleteUser")
	if !passedok {
		return
	}
	requestedPool := mux.Vars(r)["pool"]

	span.SetTag("user", requestedUser)
	span.SetTag("pool", requestedPool)

	// relevant znodes
	baseZnode := fmt.Sprintf("%s/%s/users/%s", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)
	currentZnode := fmt.Sprintf("%s/%s/users/%s/current", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)
	previousZnode := fmt.Sprintf("%s/%s/users/%s/previous", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)

	// Cannot delete user that does not exist
	if _, _, err := getZNode(baseZnode, span); err != nil {
		out := fmt.Sprintf("The user %s does not exist in pool %s", requestedUser, requestedPool)
		core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "DeleteUser", out)
		return
	}

	// stack up delete reqs
	reqs := make([]interface{}, 0)
	delreq := makeDeleteRequest(currentZnode, span)
	if delreq != nil {
		reqs = append(reqs, delreq)
	}
	delreq = makeDeleteRequest(previousZnode, span)
	if delreq != nil {
		reqs = append(reqs, delreq)
	}
	delreq = makeDeleteRequest(baseZnode, span)
	if delreq != nil {
		reqs = append(reqs, delreq)
	}

	// The were no nodes there anyway
	if len(reqs) == 0 {
		core.LogAndSetHttpStatus(w, r, http.StatusNoContent, "DeleteUser", "")
		return
	}

	// run them
	mr, err := zoo.Multi(reqs...)
	if err != nil {
		core.LogAndSetHttpStatus(w, r, http.StatusInternalServerError, "DeleteUser", "")
		log.Printf("[ERROR] Error writing ZK nodes: %v", err)
		for i, sadop := range mr {
			log.Printf("[ERROR]   (%d): %v", i, sadop.Error)
		}
		return
	}

	core.LogAndSetHttpStatus(w, r, http.StatusNoContent, "DeleteUser", "")
	ext.HTTPStatusCode.Set(span, http.StatusNoContent)
	log.Printf("[DEBUG]  DeleteUser deleted all nodes from (%s)", baseZnode)

}

func makeDeleteRequest(zNode string, span opentracing.Span) *zk.DeleteRequest {
	_, stat, err := getZNode(zNode, span)
	if err != nil {
		return nil // no existy, no problem
	}
	dr := &zk.DeleteRequest{zNode, stat.Version}
	return dr
}
