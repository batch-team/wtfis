%global debug_package %{nil}

Summary:       Small web-service holding the mapping from user to schedd. 
Name:          wtfis
Version:       0.7
Release:       3%{?dist}
License:       ASL 2.0
Source0:       https://gitlab.cern.ch/batch-team/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz
Group:         Development/Libraries
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
Prefix:        %{_prefix}
BuildRequires: golang,systemd
ExclusiveArch: x86_64
Vendor:        batch-operations@cern.ch 
Url:           https://gitlab.cern.ch/batch-team/wtfis

%description
Small web-service holding the mapping from user to schedd.

%prep
%setup -q

%build
go build -mod=vendor

%install
install -d %{buildroot}%{_bindir}
install -d %{buildroot}%{_sysconfdir}/%{name}
install -d %{buildroot}/%{_unitdir}
install -d %{buildroot}%{_sysconfdir}/sysconfig

install -p -m 0644 %{_builddir}/%{name}-%{version}/wtfis.yaml %{buildroot}%{_sysconfdir}/%{name}
install -p -m 0755 %{_builddir}/%{name}-%{version}/wtfis %{buildroot}%{_bindir}
install -p -m 0644 %{_builddir}/%{name}-%{version}/systemd/wtfis.service %{buildroot}/%{_unitdir}
install -p -m 0644 %{_builddir}/%{name}-%{version}/systemd/sysconfig.%{name} %{buildroot}%{_sysconfdir}/sysconfig/%{name}

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%{_bindir}/wtfis
%config(noreplace) /etc/wtfis/wtfis.yaml
%config(noreplace) /etc/sysconfig/wtfis
%{_unitdir}/wtfis.service
%license LICENSE

%post

# Preset on first install
%systemd_post wtfis.service

%preun

# Stops units before uninstalling
%systemd_preun wtfis.service

%postun

# Reload and restart units after installation
%systemd_postun_with_restart wtfis.service

%changelog
* Mon Jan 09 2023 Jan van Eldik <Jan.van.Eldik@cern.ch> - 0.7-3
- Move %license directive to the %files section (BBC-3315)
* Wed Dec 21 2022 Jan van Eldik <Jan.van.Eldik@cern.ch> - 0.7-2
- Build for 9
* Tue Jul 27 2021 Angeliki Kalamari <angeliki.kalamari@cern.ch> - 0.7-1
- Adding observability telemetry using Jaeger (BBC-2859)
* Thu Jun 17 2021 Luis Fernández Álvarez <luis.fernandez.alvarez@cern.ch> - 0.6-2
- Build for 8s
* Fri Sep 11 2020 Ben Jones <ben.dylan.jones@cern.ch> - 0.6-1
- Error handling in GetUser
- Don't fall back to legacy anymore
* Thu Mar 19 2020 Ben Jones <ben.dylan.jones@cern.ch> - 0.5-1
- Only fall back to legacy in default pool
* Mon Mar 16 2020 Ben Jones <ben.dylan.jones@cern.ch> - 0.4-1
- Fall back to legacy path
* Wed Mar  4 2020 Luis Fernández Álvarez <luis.fernandez.alvarez@cern.ch> - 0.3-2
- Improved packaging (BBC-2645)
