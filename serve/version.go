package serve

import (
	"encoding/json"
	"net/http"

	"github.com/opentracing/opentracing-go/ext"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

func handleVersion(w http.ResponseWriter, r *http.Request) {
	span := core.ExtractSpanFromHeaders("Version", r)
	defer span.Finish()

	_, _, _, passedok := core.RequireUserIsAdmin(w, r, "Version")
	if !passedok {
		return
	}

	core.LogAndSetHttpStatus(w, r, http.StatusOK, "Version", "")
	ext.HTTPStatusCode.Set(span, http.StatusOK)

	verj := make(map[string]string)
	verj["Version"] = core.SERVER_VERSION
	w.Header().Set("Content-Type", "application/json")

	verjson, _ := json.Marshal(verj)
	w.Write(verjson)

}
