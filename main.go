package main

import "gitlab.cern.ch/batch-team/wtfis/cmd"

func main() {
	cmd.Execute()
}
