// Copyright © 2018 NAME HERE <EMAIL ADDRESS>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"

	"github.com/fsnotify/fsnotify"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
	"gitlab.cern.ch/batch-team/wtfis/serve"
)

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the server",
	Long:  `Start the server.`,
	Run:   doServe,
}

var (
	DefaultZkConn   = []string{"localhost:2181"}
	DefaultAcl      []string
	DefaultRootPath = "/wtfis"
)

func init() {
	setServeDefaults()
	rootCmd.AddCommand(serveCmd)

	serveCmd.Flags().Int("port", 8000, "Port to listen on")
	err := viper.BindPFlag(core.SERVE_PORT_KEY, serveCmd.Flags().Lookup("port"))
	if err != nil {
		log.Printf("[ERROR] could not fetch the [port] argument. error [%v]", err)
	}

	serveCmd.Flags().StringSlice("zkconn", DefaultZkConn, "Zookeeper connection string (multiple)")
	err = viper.BindPFlag(core.SERVE_ZK_CONN, serveCmd.Flags().Lookup("zkconn"))
	if err != nil {
		log.Printf("[ERROR] could not fetch the [zkconn] argument. error [%v]", err)
	}

	serveCmd.Flags().StringSlice("acl", DefaultAcl, "ACL list of admin users (multiple)")
	err = viper.BindPFlag(core.SERVE_ACL_KEY, serveCmd.Flags().Lookup("acl"))
	if err != nil {
		log.Printf("[ERROR] could not fetch the [acl] argument. error [%v]", err)
	}

	serveCmd.Flags().String("rootpath", DefaultRootPath, "Root path in backing store")
	err = viper.BindPFlag(core.SERVE_ROOTPATH, serveCmd.Flags().Lookup("rootpath"))
	if err != nil {
		log.Printf("[ERROR] could not fetch the [rootpath] argument. error [%v]", err)
	}

	serveCmd.Flags().String("getauthmode", "all", "Authz mode for getuser operations ('all' or 'admin')")
	err = viper.BindPFlag(core.AUTHZ_GETMODE, serveCmd.Flags().Lookup("getauthmode"))
	if err != nil {
		log.Printf("[ERROR] could not fetch the [getauthmode] argument. error [%v]", err)
	}

}

func doServe(cmd *cobra.Command, args []string) {
	err, tracer, closer := core.InitTracing()
	if err == nil {
		log.Printf("[INFO] Initialising tracer")
		defer closer.Close()
		opentracing.SetGlobalTracer(tracer)
	}

	logServeConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Printf("[INFO] Re-read config file after update: %s", e.Name)
		logServeConfig()
	})

	if err := serve.Run(); err != nil {
		log.Fatal("[FATAL] Could not start server: ", err)
	}
}

func logServeConfig() {
	log.Printf("[INFO] Serve Configuration Values:")
	log.Printf("[INFO] \t%s: %d", core.SERVE_PORT_KEY, viper.GetInt(core.SERVE_PORT_KEY))
	log.Printf("[INFO] \t%s: %s", core.SERVE_ZK_CONN, viper.GetStringSlice(core.SERVE_ZK_CONN))
	log.Printf("[INFO] \t%s: %s", core.SERVE_ACL_KEY, viper.GetStringSlice(core.SERVE_ACL_KEY))
	log.Printf("[INFO] \t%s: %s", core.SERVE_ROOTPATH, viper.GetString(core.SERVE_ROOTPATH))
	log.Printf("[INFO] \t%s: %s", core.AUTHZ_GETMODE, viper.GetString(core.AUTHZ_GETMODE))

}

func setServeDefaults() {
	viper.SetDefault(core.SERVE_PORT_KEY, 8000)
	viper.SetDefault(core.SERVE_ZK_CONN, DefaultZkConn)
	viper.SetDefault(core.SERVE_ACL_KEY, DefaultAcl)
	viper.SetDefault(core.SERVE_ACL_KEY, DefaultRootPath)
	viper.SetDefault(core.AUTHZ_GETMODE, "all")
}
