package serve

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/go-zookeeper/zk"
	"github.com/gorilla/mux"
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

func existingZNode(znode string, span opentracing.Span) (bool, *zk.Stat, error) {
	span = opentracing.GlobalTracer().StartSpan(
		"existing-znode",
		opentracing.ChildOf(span.Context()),
	)
	defer span.Finish()

	span.SetTag("znode", znode)
	return zoo.Exists(znode)
}

func handleActiveUser(w http.ResponseWriter, r *http.Request) {
	span := core.ExtractSpanFromHeaders("ActiveUser", r)
	defer span.Finish()

	requestedUser, _, _, passedok := core.RequireUserIsAdmin(w, r, "PutActiveUser")
	if !passedok {
		return
	}
	requestedPool := mux.Vars(r)["pool"]

	span.SetTag("user", requestedUser)
	span.SetTag("pool", requestedPool)

	// decode message and 500 if it's bad
	var activeMessage core.ActiveUserMessage
	if r.Body == nil { //todo: actually wrong
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "ActiveUser", "Missing message body.")
		return
	}
	err := json.NewDecoder(r.Body).Decode(&activeMessage)
	if err != nil {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "ActiveUser", "Invalid JSON body")
		return
	}

	// check all the required fields are set and 500 if they are not
	// NB. empty ActiveSchedds is OK
	if activeMessage.User == "" || activeMessage.Pool == "" {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "ActiveUser", "Not all required fields passed in JSON")
		return
	}

	// check the user in body matches the user requested and 500 if not
	if activeMessage.User != requestedUser {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "ActiveUser", "User passed in JSON does not match user in PUT request URL.")
		return
	}

	// check the pool in body matches the pool requested and 500 if not
	if activeMessage.Pool != requestedPool {
		core.LogAndSetHttpStatus(w, r, http.StatusBadRequest, "ActiveUser", "Pool passed in JSON does not match pool in PUT request URL.")
		return
	}

	// relevant znodes
	currentZnode := fmt.Sprintf("%s/%s/users/%s/current", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)
	previousZnode := fmt.Sprintf("%s/%s/users/%s/previous", viper.GetString(core.SERVE_ROOTPATH), requestedPool, requestedUser)

	// check user is even there
	exists, _, _ := existingZNode(currentZnode, span)
	if !exists {
		out := fmt.Sprintf("Requested user (%s) is not known in pool %s", requestedUser, requestedPool)
		core.LogAndSetHttpStatus(w, r, http.StatusNotFound, "ActiveUser", out)
		return
	}

	// get old value, of 'current' node. 'previous' znode not needed as it will be overwritten.
	current, _, _ := getZNode(currentZnode, span)

	// active array from message
	new_Active_array := activeMessage.ActiveSchedds

	// create new previous array to the Set(new_Active_array - current)
	pset := make(map[string]int)
	for _, p := range new_Active_array {
		pset[p] = 0
	}
	delete(pset, string(current))

	new_Previous_array := make([]string, 0)
	for p, _ := range pset {
		new_Previous_array = append(new_Previous_array, p)
	}

	// ..back to space separated string
	new_Previous := strings.Join(new_Previous_array, " ")

	// update the previous zNode with the new value, abusing the polymorph of Multi
	previousOp := createOrSetOp(previousZnode, new_Previous)

	mr, err := zoo.Multi(previousOp)
	if err != nil {
		core.LogAndSetHttpStatus(w, r, http.StatusInternalServerError, "ActiveUser", "")
		log.Printf("[ERROR] Error writing ZK nodes: %v", err)
		for i, sadop := range mr {
			log.Printf("[ERROR]   (%d): %v", i, sadop.Error)
		}
		return
	}

	core.LogAndSetHttpStatus(w, r, http.StatusOK, "ActiveUser", "")
	ext.HTTPStatusCode.Set(span, http.StatusOK)
	log.Printf("[DEBUG]  ActiveUser updated (%s) to (%s)", previousZnode, new_Previous)

}
