package serve

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/go-zookeeper/zk"
	"github.com/pkg/errors"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

var wg sync.WaitGroup
var zoo *zk.Conn // really..

func Run() error {
	hostname, err := os.Hostname()
	if err != nil {
		return errors.Wrap(err, "could not get the hostname value")
	}

	log.Printf("[INFO] Serve started on %s", hostname)

	globalCtx, cancel := context.WithCancel(context.WithValue(context.Background(), core.WG_CTX_KEY, &wg))

	// connect to Zookeeper, take care, zoo is package scope
	zoo, err = core.ConnectZK()
	if err != nil {
		cancel()
		return err
	}

	// start HTTP listener for client commands (expecting authenticated REMOTE_USER)
	SetupHttpClientHandler(globalCtx)

	// ordered shutdown once context is canceled
	wg.Add(1)
	go func() {
		defer wg.Done()
		<-globalCtx.Done()
		log.Printf("[DEBUG] Shutting down connections to database")
		zoo.Close()
	}()

	// block until we're signalled
	signaled := make(chan os.Signal)
	signal.Notify(signaled, syscall.SIGINT, syscall.SIGTERM)
	select {
	case <-signaled:
		log.Printf("[DEBUG] Got local termination signal. Stopping.")
	}

	// cancel context and wait for cleanup
	cancel()

	// ..hang about till we're done
	log.Printf("[DEBUG] Waiting for handlers to shut down")
	wg.Wait()
	log.Printf("[INFO] Serve stopped on %s", hostname)

	return nil
}
