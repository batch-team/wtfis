package cmd

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "wtfis",
	Short: "Small web-service holding the mapping from user to schedd",
	Long: `Small web-service holding the mapping from user to schedd.
Expects to be run behind a web-server providing an authenticated REMOTE_USER.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Config file
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/wtfis.yaml)")

	// Debug
	rootCmd.PersistentFlags().BoolP("debug", "d", false, "Switch on debug messages.")
	viper.BindPFlag("debug", rootCmd.PersistentFlags().Lookup("debug"))

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name "wtfis" (without extension).
		viper.AddConfigPath(home)
		viper.AddConfigPath("/etc/wtfis")
		viper.SetConfigName("wtfis")
	}

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.SetEnvPrefix("WTFIS")
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		log.Printf("[INFO] Using config file: %s", viper.ConfigFileUsed())
	} else {
		log.Printf("[ERROR] Could not read the configuration file: %s", err)
	}

	//viper.WatchConfig()

	// setup logging
	core.SetupLogger()

	// setup pool
	core.SetupPools()

}
