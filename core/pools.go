package core

import (
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	"log"
)

type PoolMap struct {
	Pools map[string]ScheddList
}

type ScheddList struct {
	Standard []string
	Restricted []string
}

func SetupPools() {
	var pm PoolMap
	pools := viper.GetStringMap("pools")
	if pools == nil {
		log.Printf("[ERROR] Pool comfiguration is empty")
		return
	}
	// make it match the desired struct
	ppools := make(map[string]interface{})
	ppools["pools"] = pools

	// dump the YAML [string]interface onto the struct
	err := mapstructure.Decode(ppools, &pm)
	if err != nil {
		log.Printf("[ERROR] Pool comfiguration cannot be parsed: %v", err)
		return
	}

	viper.Set(INTERNAL_POOLMAP, &pm)
	ap := make([]string, 0)
	for thispool, _ := range pm.Pools {
		ap = append(ap, thispool)
	}
	viper.Set(INTERNAL_POOLS, ap)
	log.Printf("[INFO] Configured pools: %s", ap)
}