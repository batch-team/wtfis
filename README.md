## WTFIS

Small web-service holding the mapping from user to schedd. It allows users to find their currently assigned schedd (together with their previous ones) and to move the mapping to a new one.

It's a JSON REST web-service, assuming Apache with mod_auth_krb providing the authenticated REMOTE_USER.

Its backend is ZooKeeper.

Providing API:

 * Pool is the HTCondor pool in question e.g. tzero vs. share. The users may be the same, but the 
   schedds are different.

 * **GET /{pool}/user/{user}**

  Returns current and old schedds for the specified user.
  
  A user may query himself. An admin may query anyone.
  
  Returns HTTP/200 on success with JSON body:

```JSON
     {
       "user":"bob",
       "currentschedd":"scheddo01.cern.ch",
       "oldschedds": [
          "scheddo02.cern.ch",
          "scheddo03.cern.ch"
        ]
     }
  ```

 * **GET /{pool}/schedd/{schedd}**

  Gets the list of users whose current schedd is set to the requested one.

  Admin-only operation.

  Returns HTTP/200 on sucesss with JSON body:

  ```JSON
       {
         "schedd": "scheddo01.cern.ch",
         "users": [
            "bob",
            "john",
            "roger"
          ]
       }
  ```

 * **PUT /{pool}/user/{user}**

   Updates the current value and places the old value on the list of old ones.

   User in body must be the same as user in URL.
   
   A user may update his own schedd. An admin may update anyone's schedd.

   Returns empty HTTP/200 on success.

   Required JSON in body is:

```JSON
     {
       "user": "shabba",
       "currentschedd": "scheddo01.cern.ch"
     }
```

 * **PUT /{pool}/user/{user}/active**

   Post list of all schedds holding active. Updates the 'previous' list to this (minus the current one).

   Admin-only operation to be called periodically.

   User in body must be the same as user in URL.

   Returns empty HTTP/200 on success.

   Required JSON in body is:

```JSON
   {
     "user": "shabba",
     "activeschedds": [
        "scheddo01.cern.ch",
        "scheddo02.cern.ch"
      ]
   }
```

 * **DELETE /{pool}/user/{user}**

   Admin-only idempotent operation that deletes the given user and mappings.

   Returns an empty HTTP/204 on success.


## Packaging & Release

The basic steps for the development and release process are:
 1. Add your modifications via merge requests. Each MR will trigger a pipeline that validates the packaging process.
 2. Once you have merged the new features and a new release is needed:
     1. Update the RPM spec file with the new version, as well as an entry in the changelog.
     2. Create a new merge request for these modifications and validate the generated RPM.
 3. When the previous MR is merged, you can tag the merged commit. A new release pipeline will be triggered automatically.
     * `git tag -am "Release 1.0-1" 1.0-1`
     * `git push origin 1.0-1`
 4. The release pipeline will tag automatically the build in 'testing'. The tagging to 'qa' / 'stable' are manual steps to be triggered by the developer.
     * Example: https://gitlab.cern.ch/batch-team/wtfis/pipelines/1489345

## Tests

There is a branch called `tests` in this repository that contains automated tests run by GitLabCI.
