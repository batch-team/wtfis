package serve

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"gitlab.cern.ch/batch-team/wtfis/core"
	"log"
	"net/http"
	"os"
	"sync"
	"time"
)

// Setup HTTP connection to handle client connections
func SetupHttpClientHandler(ctx context.Context) {

	// set up routes
	r := mux.NewRouter()
	r.HandleFunc("/version", handleVersion).Methods("GET")
	r.HandleFunc("/{pool}/user/{user}", handleGetUser).Methods("GET")
	r.HandleFunc("/{pool}/user/{user}", handlePutUser).Methods("PUT", "POST")
	r.HandleFunc("/{pool}/user/{user}", handleDeleteUser).Methods("DELETE")
	r.HandleFunc("/{pool}/user/{user}/active", handleActiveUser).Methods("PUT", "GET")
	r.HandleFunc("/{pool}/schedd/{schedd}", handleGetSchedd).Methods("GET")

	port := viper.GetInt(core.SERVE_PORT_KEY)
	portString := fmt.Sprintf(":%d", port)

	log.Printf("[INFO] Creating the HTTP server on address [%s]", portString)
	srv := &http.Server{Addr: portString, Handler: r}

	// listen until the server has been shutdown
	wg := ctx.Value(core.WG_CTX_KEY).(*sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()
		log.Printf("[INFO] Started client HTTP Handler on port [%d]", port)
		if err := srv.ListenAndServe(); err != http.ErrServerClosed {
			log.Printf("[FATAL] Could not start HTTP server. Error [%v]", err )
			os.Exit(1)
		}
		log.Printf("[INFO] Stopped client HTTP Handler on port [%d]", port)
	}()

	// shutdown the server, with timeout, when the global context is canceled
	go func() {
		<-ctx.Done()
		log.Printf("[INFO] Gracefully terminating client HTTP handler (10 secs..)")
		cc, _ := context.WithTimeout(context.Background(), 10*time.Second)
		srv.Shutdown(cc)
	}()
}
