FROM golang:latest as build

WORKDIR /go/src/gitlab.cern.ch/batch-team/wtfis
COPY . ./

RUN GO111MODULE=on CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -o wtfis main.go

FROM alpine:latest
COPY --from=build /go/src/gitlab.cern.ch/batch-team/wtfis/wtfis /usr/bin/
