//+build !test

package ci

import (
	"encoding/json"
	"fmt"
	"gitlab.cern.ch/batch-team/wtfis/tests/helpers"
	"net/http"
	"testing"
)

//const ApiUrl = "https://wtfis-qa.cern.ch"
const ApiUrl = "http://localhost:8000"

/*

	@TODO Will need 2 service accounts ideally: admin vs. user operations

	go run main.go serve --zkconn 10.254.40.223:2181
	go run main.go serve --zkconn 10.254.40.223:2181 --acl paaperei --standardschedd scheddo01.cern.ch --standardschedd activeschedd.cern.ch

	@requires libkrb5-dev
 */



const (
	testAdmin           string = "paaperei"
	testPool				   = "share"
	allowedSchedd              = "scheddo01.cern.ch"
	activeAllowedSchedd        = "activeschedd.cern.ch"
)

const (
	testUser			string = "paaperei_test"
)

type userSchema struct {
	User, Pool, CurrentSchedd string
	OldSchedds 				  []string
}

type putActiveSchema struct {
	User				string
	Pool                string
	ActiveSchedds 		[]string
}

func ToJSON(i interface{}) string {
	out, _ := json.Marshal(i)
	return string(out)
}


func TestUserPermissions(t *testing.T) {
	// Create the test user via admin
	helpers.RunMultiple(t,
		helpers.HttpTest{
			Name:				"PUT :: Authorized test user creation :: /{pool}/user/{user}",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodPut,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testUser),
			Data: 				ToJSON(userSchema{Pool: testPool, User: testUser, CurrentSchedd:allowedSchedd}),
			ExpectedResponse:
			helpers.WTFISResponse{
				StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusOK}},
		},
		helpers.HttpTest{
			Name:				"PUT :: Authorized test user creation, but on a bad pool :: /abadpool/user/{user}",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodPut,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s", ApiUrl, "abadpool", testUser),
			Data: 				ToJSON(userSchema{Pool: "abadpool", User: testUser, CurrentSchedd:allowedSchedd}),
			ExpectedResponse:
			helpers.WTFISResponse{
				StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusNotFound}},
		},
		)

	helpers.RunMultiple(t,
		// Ascertain that the user can query itself
		helpers.HttpTest{
			Name:				"GET :: Self-query :: /{pool}/user/{user}",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodGet,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testUser),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusOK},
					JsonBody:		helpers.PartResponse{Enabled:true, Field:
						ToJSON(userSchema{Pool: testPool, User: testUser, CurrentSchedd:allowedSchedd, OldSchedds:[]string{}}) + "\n"},
				},
		},
		// Ascertain that the user cannot query other users
		// UPDATED: with default 'all' option to getauthzmode, this should now be allowed
		helpers.HttpTest{
			Name:				"GET :: Now it's an authorized query :: /{pool}/user/{user}",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodGet,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusNotFound}},
		})

	// Update of the active schedd list it not allowed //
	helpers.RunMultiple(t,
		// Self
		helpers.HttpTest{
			Name:				"PUT :: Unauthorized schedd trim:: /{pool}/user/{user}/active",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodPut,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s/active", ApiUrl, testPool, testUser),
			Data: ToJSON(putActiveSchema{Pool: testPool, User: testUser, ActiveSchedds: []string{activeAllowedSchedd}}),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusUnauthorized}},
		},
		// Others
		helpers.HttpTest{
			Name:				"PUT :: Unauthorized an schedd trim for other users :: /{pool}/user/{user}/active",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:  			http.MethodPut,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s/active", ApiUrl, testPool, testAdmin),
			Data: 				ToJSON(putActiveSchema{Pool: testPool, User: testAdmin, ActiveSchedds: []string{activeAllowedSchedd}}),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusUnauthorized}},
		})


	// Deletion is not allowed for the test user
	helpers.RunMultiple(t,
		// Self
		helpers.HttpTest{
			Name:				"DELETE :: Unauthorized delete the test user :: /{pool}/user/{user}",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodDelete,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testUser),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusUnauthorized}},
		},
		// Others
		helpers.HttpTest{
			Name:				"DELETE :: Unauthorized delete of other users :: /{pool}/user/{user}",
			RemoteUserHeader: 	testUser,
			IgnoreCertificate: 	true,
			Method:   			http.MethodDelete,
			Endpoint: 			fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusUnauthorized}},
		},
		// Delete the test user via admin
		helpers.HttpTest{
			Name:             	"DELETE :: Delete the test user via admin :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodDelete,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testUser),
			ExpectedResponse: 		helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusNoContent}},
		})
}

func TestUserSchedds(t *testing.T) {
	/*
		Goal : Returns current and old schedds for the specified user
			1) A user may query himself
			2) Admin may query anyone
			3) http.StatusOk is returned upon success
	 */


	helpers.RunMultiple(t,
		// Ascertain that the user does not exist at the start of the test
		helpers.HttpTest{
			Name:             	fmt.Sprintf("GET :: Before user creation :: /{pool}/user/%s", testAdmin),
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodGet,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse:
				helpers.WTFISResponse{
					StatusCode:		helpers.PartResponse{Enabled:true, Field:http.StatusNotFound}},
		},
		// Create the user
		helpers.HttpTest{
			Name:             "PUT :: User creation :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodPut,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			Data:             	ToJSON(userSchema{Pool: testPool, User: testAdmin, CurrentSchedd:allowedSchedd}),
			ExpectedResponse: 		helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusOK}},
		},
		// Ascertain that the user now does in fact exist
		helpers.HttpTest{
			Name:             "GET :: After user creation :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodGet,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse: 	helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusOK},
				JsonBody:			helpers.PartResponse{Enabled:true, Field:
					ToJSON(userSchema{Pool: testPool, User: testAdmin, CurrentSchedd:allowedSchedd, OldSchedds:[]string{}}) + "\n"},
			},
		},
		// Test that a current user can be updated with a new value for currentschedd
		helpers.HttpTest{
			Name:             "PUT :: Update user :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodPut,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			Data:             	ToJSON(userSchema{Pool: testPool, User: testAdmin, CurrentSchedd:allowedSchedd}),
			ExpectedResponse: 	helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusOK}},
		},
		// Attempt to call the [/active] endpoint
		helpers.HttpTest{
			Name:             "PUT :: Trim the active list :: /{pool}/user/{user}/active",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:          	http.MethodPut,
			Endpoint:        	fmt.Sprintf("%s/%s/user/%s/active", ApiUrl, testPool, testAdmin),
			Data:            	ToJSON(putActiveSchema{Pool: testPool, User: testAdmin, ActiveSchedds: []string{activeAllowedSchedd}}),
			ExpectedResponse: 	helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusOK}},
		},
		// Ascertain that the added schedd was attached to the user
		helpers.HttpTest{
			Name:             "GET :: After adding active schedd :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodGet,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse: 	helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusOK},
				JsonBody:			helpers.PartResponse{Enabled:true, Field:
				ToJSON(userSchema{Pool: testPool, User: testAdmin, CurrentSchedd:allowedSchedd, OldSchedds:[]string{activeAllowedSchedd}}) + "\n"},
			},
		},
		// Delete the user
		helpers.HttpTest{
			Name:             "DELETE :: Delete the created user :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodDelete,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse: 		helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusNoContent}},
		},
		// Ascertain that the user no longer exists
		helpers.HttpTest{
			Name:             "GET :: After user deletion :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodGet,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse: 		helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusNotFound}},
		},
		// Ascertain that a non-existing user cannot be deleted if it does not exist
		helpers.HttpTest{
			Name:             "DELETE :: Cannot delete if doesn't exist :: /{pool}/user/{user}",
			RemoteUserHeader: 	testAdmin,
			IgnoreCertificate: 	true,
			Method:           	http.MethodDelete,
			Endpoint:         	fmt.Sprintf("%s/%s/user/%s", ApiUrl, testPool, testAdmin),
			ExpectedResponse: 		helpers.WTFISResponse{
				StatusCode:			helpers.PartResponse{Enabled:true, Field:http.StatusNotFound}},
		},)
}
